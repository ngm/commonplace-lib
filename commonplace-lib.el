(defun commonplace/get-title (file)
  "For a given file, get its TITLE keyword."
  (with-current-buffer
      (get-file-buffer file)
    (cadar (org-collect-keywords '("TITLE")))))

(defun commonplace/slugify-title (title)
  "Convert TITLE to a filename-suitable slug.  Use hyphens rather than underscores."
  (cl-flet* ((nonspacing-mark-p (char)
                                (eq 'Mn (get-char-code-property char 'general-category)))
             (strip-nonspacing-marks (s)
                                     (apply #'string (seq-remove #'nonspacing-mark-p
                                                                 (ucs-normalize-NFD-string s))))
             (cl-replace (title pair)
                         (replace-regexp-in-string (car pair) (cdr pair) title)))
    (let* ((pairs `(("['\?,%]" . "")
                    ("[^[:alnum:][:digit:]]" . "-")  ;; convert anything not alphanumeric
                    ("--*" . "-")  ;; remove sequential underscores
                    ("^-" . "")  ;; remove starting underscore
                    ("-$" . "")))  ;; remove ending underscore
           (slug (-reduce-from #'cl-replace (strip-nonspacing-marks title) pairs)))
      (downcase slug))))

(provide 'commonplace-lib)
